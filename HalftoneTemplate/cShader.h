//////////////////////////////////////////////////////////////////////////////////////////
// cShader.h
//////////////////////////////////////////////////////////////////////////////////////////

#pragma once

class cShader
{
public:

	cShader(){}
	~cShader(){}

	inline void		cwd(char *path){ strcpy( m_cwd, path );}
	inline char		*cwd(){return m_cwd;}
	
	inline u32		program(){return m_program;}
	inline GLint	get_grabLoc(){return m_grabLoc;}
	inline GLfloat  gridSize() { return m_gridSize; }
	inline GLint screenWidth() { return m_screenWidth; }
	inline GLint screenHeight() { return m_screenHeight; }

	inline GLfloat  lightPosX() { return m_LightPosX; }
	inline GLfloat  lightPosY() { return m_LightPosY; }
	inline GLfloat  lightPosZ() { return m_LightPosZ; }

	inline GLfloat  camPosX() { return m_CamPosX; }
	inline GLfloat  camPosY() { return m_CamPosY; }
	inline GLfloat  camPosZ() { return m_CamPosZ; }

	inline GLfloat  lightR() { return m_LightR; }
	inline GLfloat  lightG() { return m_LightG; }
	inline GLfloat  lightB() { return m_LightB; }

	void clean(char *pVertexShader_path, char *pFragmentShader_Path);
	void create(char *pVertexShader_path, char *pFragmentShader_Path);

private:
	char m_vpath[256];
	char m_fpath[256];
	char m_cwd[256];

	GLuint m_v;
	GLuint m_f;

	u32 m_program;

	GLint	m_grabLoc;
	GLfloat m_gridSize;
	GLint m_screenWidth;
	GLint m_screenHeight;

	GLfloat m_LightPosX;
	GLfloat m_LightPosY;
	GLfloat m_LightPosZ;

	GLfloat m_CamPosX;
	GLfloat m_CamPosY;
	GLfloat m_CamPosZ;

	GLfloat m_LightR;
	GLfloat m_LightG;
	GLfloat m_LightB;
};


class cShaderInfo
{
public:

	cShaderInfo();
	~cShaderInfo()
	{
	//	delete m_pList;
	}
	void create();//int count);
	void clean();

	inline cShader	*getList(){	return m_pList;}

	cShader *list(int id);

	void setOGL2support(bool value);
	bool isOGL2supported();

	void set_flags(u32 value)
	{
		m_flags = value;
	}

	u32 get_flags()
	{
		return m_flags;
	}

	inline void shaderCount(int count){ m_num_shaders = count;}
	inline int  shaderCount(){return m_num_shaders;}
	

private:
	cShader		*m_pList;

	int			m_num_shaders;
	u32			m_flags;		// used to render individual maps
	
//	bool		m_OpenGL2_supported;
};