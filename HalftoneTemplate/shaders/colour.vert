//COLOUR.VERT

//per vertex calculations for colour.frag

varying vec2 	texCoord;
uniform float gridSize;
uniform int pass;
uniform int screenWidth;
uniform int screenHeight;

void main(void)
{
	//set position
	gl_Position 	= ftransform();
	//get texture coord
	texCoord	= gl_MultiTexCoord0.xy;
}