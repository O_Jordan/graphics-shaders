//CIRCLES.VERT

//Per vertex calc for circles.frag

varying vec2 	texCoord;
uniform float gridSize;
uniform int pass;
uniform int screenWidth;
uniform int screenHeight;

void main(void)
{	
	//setting position
	gl_Position 	= ftransform();
	//setting texture coord
	texCoord	= gl_MultiTexCoord0.xy;
}