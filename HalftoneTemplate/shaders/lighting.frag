//LIGHTING.FRAG

//Performs blinn-phong shading on 3D objects in a scene - currently only running for one light source...

varying vec4 objColour;
varying vec3 normal;
varying vec4 m_vertex;
varying vec4 m_position;

uniform float m_CamPosX;
uniform float m_CamPosY;
uniform float m_CamPosZ;
void main(void)
{
	//hard coded lighting strengths and colours...
	float specStrength = 0.5;
	float shininess = 32;
	float ambient = 0.25;
	vec3 lightColour = vec3(0.75, 0.75, 0.75);
	
	//calculate ambient intensity of object...
	vec3 ambInt = vec3(objColour.xyz * ambient);
	
	//grab light position...
	vec3 lightPos = vec3(gl_LightSource[0].position.xyz);
	
	//normalize fragment normal...
	vec3 norm = normalize(normal);
	
	//calculate light direction unit vector...
	vec3 lightDir = vec3(normalize(lightPos - m_position.xyz));
	
	//calculate view direction unit vector...
	vec3 viewDir = vec3(normalize(vec3(m_CamPosX,m_CamPosY,m_CamPosZ) - m_position.xyz));
	
	//calculate halfway unit vector...
	vec3 halfDir = vec3(normalize(lightDir + viewDir));
	
	//calculate diffuse value for this frag...
	float diff = max(dot(norm, lightDir),0.0);
	
	//multiply by light colour
	vec3 diffuse = diff * lightColour;
	
	//calculate specular value for this frag...
	float spec = pow(max(dot(norm,halfDir), 0.0), shininess);
	
	//multiply by light colour...
	vec3 specInt = spec * lightColour;
	
	//resulting fragment colour is object colour multiplied by sum of all lighting vectors...
	vec3 colour = vec3(objColour.xyz * (ambient+diffuse + specInt));
	
	//set fragment to result...
	gl_FragColor = vec4(colour,1);
	
	
}
