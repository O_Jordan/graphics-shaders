//CIRCLES.FRAG

//At each frag, checks avg intensity and colours pixel accordingly to achieve halftone effect

uniform sampler2D	grabTexture;
uniform float gridSize;
varying vec2 		texCoord;

uniform int pass;
uniform int screenWidth;
uniform int screenHeight;
void main(void)
{
	//grab texture at this coord
	vec4	grab = vec4(texture2D(grabTexture, texCoord.xy));
	
	//scale coords to screen space
	float scaledX = texCoord.x * screenWidth;
	float scaledY = texCoord.y * screenHeight;
	
	//init vector to store intensity total
	vec4 intensityTot = vec4(0,0,0,0);
	
	//radius of circle at max size is half of the grid size
	float radius = gridSize/2;
	
	//offset from left upper corner of each grid (where intensity pixel is stored) can be found by calculation coord modulo gridsize - remainder is how far into the grid we are
	float offsetX = mod(scaledX, gridSize);
	float offsetY = mod(scaledY, gridSize);
	
	//scaled - offset gets left edge and upper edge of the grid cell - adding radius to each gives the centre pixel
	float centreX = (scaledX - offsetX) + radius;
	float centreY = (scaledY - offsetY) + radius;
	
	//tograb variables are screen space coords of top left
	float toGrabX = scaledX - offsetX;
	float toGrabY = scaledY - offsetY;
	
	//scale to texture space
	float scaledGrabX = toGrabX/screenWidth;
	float scaledGrabY = toGrabY/screenHeight;
	
	//storing it in vector
	vec2 grabbing = vec2(scaledGrabX, scaledGrabY);
	
	//grabbing intensity pixel and storing it 
	vec4 avgIntens = vec4(texture2D(grabTexture, grabbing.xy));
	
	//averaging each colour channel intensity
	float totAvgInts = (avgIntens.x + avgIntens.y + avgIntens.z)/3;
	
	//first part of distance calculation using pythag
	float rootThis = ((centreX - scaledX)*(centreX - scaledX))+((centreY - scaledY) * (centreY - scaledY));
	
	//second part of pythag distance calc 
	float dist = sqrt(rootThis);
	
	//setting colour vector
	vec4 colour = vec4(0,0,0,1);
	
	//if pixel is within a scaled radius...
	if(dist < radius*totAvgInts)
	{
		//becomes white
		colour = vec4(1,1,1,1);
	}
	//if pixel is outside scaled radius...
	else
	{
		//becomes black
		colour = vec4(0,0,0,1);
	}
	
	//setting the fragment colour...
	gl_FragColor = colour;
}