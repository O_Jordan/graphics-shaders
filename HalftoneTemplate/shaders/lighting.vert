//LIGHTING.VERT

//per vector calculations for lighting.frag

varying vec4 objColour;
varying vec3 normal;
varying vec4 m_vertex;
varying vec4 m_position;

uniform float m_CamPosX;
uniform float m_CamPosY;
uniform float m_CamPosZ;
void main(void)
{	
	//grab this vertex's normal multiply it by openGL normal matrix
	normal = gl_NormalMatrix * gl_Normal;
	
	//store this vertex...
	m_vertex = gl_Vertex;
	
	//store this colour...
	objColour = gl_Color;
	
	//store position...
	gl_Position 	= ftransform();
	
	//calculate position in world space...
	m_position = vec4(gl_ModelViewMatrix * gl_Vertex);
}