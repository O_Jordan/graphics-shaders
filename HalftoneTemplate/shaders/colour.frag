//COLOUR.FRAG

//Scans screen and grabs average intensity for each cell in a grid, stores it in the top left pixel of each cell

uniform sampler2D	grabTexture;
uniform float gridSize;
varying vec2 		texCoord;
uniform int pass;
uniform int screenWidth;
uniform int screenHeight;
void main(void)
{
	//grabbing from texture...
	vec4	grab = vec4(texture2D(grabTexture, texCoord.xy));
	
	//Scaling to image space
	float scaledX = texCoord.x * screenWidth;
	float scaledY = texCoord.y * screenHeight;
	
	//setting storage for intensity value
	vec4 intensityTot = vec4(0,0,0,0);
	
	//setting colour storage...
	vec4 colour = vec4(0,0,0,1);
	
	//if modulus of coords both return zero - we are on top left pixel of a cell. Time to grab intensity values!
	if((mod(scaledX, gridSize) == 0) && (mod(scaledX, gridSize) == 0))
	{
		//for every value along x in this cell...
		for(int i = 0; i < gridSize; i++)
		{
			//for every value along y in this cell...
			for(int j = 0; j < gridSize; j++)
			{
				//store pixel coord...
				vec2 pixToGrab = vec2(scaledX + i, scaledY + j);
				
				//scale to texture space
				vec2 scaled = vec2(pixToGrab.x/screenWidth, pixToGrab.y/screenHeight);
				
				//grab pixel from texture
				vec4 grabbed = vec4(texture2D(grabTexture, scaled.xy));
				
				//add pixel value to intensity total
				intensityTot = intensityTot + grabbed;
			}
		}
		
		//average intensity value then becomes intensity total divided by total cell pixels
		intensityTot = vec4(intensityTot/ (gridSize*gridSize));
		
		//in case something goes wrong and colour values are more than 1, default to a white pixel so as not to crash shader
		//(irreleavnt check because a scuffed value here produces incorrect shader result anyway? Mainly a debug thing in that case - visual error checking)
		if(intensityTot.x > 1 || intensityTot.y > 1 || intensityTot.z > 1)
		{
			//set colour to white
			colour = vec4(1,0,0,1);
		}
		//everything fine? Good, then...
		else
		{
			//set colour to average intensity value
			colour = vec4(vec3(intensityTot.xyz),1);
		}
		//set the fragment colour to the decided colour result
		gl_FragColor = colour;
	}
	else
	{
	//if we aren't on top left pixel we can just draw the image as it is - set frag to grabbed pixel values
		gl_FragColor = grab;
	}

}