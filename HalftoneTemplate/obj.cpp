#include "obj.h"
#include "stdafx.h"
obj::obj(int inVSize, int inVTSize, int inVNSize, int inFSize)
{
	//set sizes
	vSize = inVSize;
	vtSize = inVTSize;
	vnSize = inVNSize;
	fSize = inFSize;

	//set up lists
	vList = new Vertex[vSize];
	vtList = new vTexture[vtSize];
	vnList = new vNormal[vnSize];
	fList = new Face[fSize];

	//filling vertex list with empties
	for (int i = 0; i < vSize; i++)
	{
		Vertex clean;
		clean.x = 0.0f;
		clean.y = 0.0f;
		clean.z = 0.0f;
		vList[i] = clean;
	}
}

obj::~obj()
{

}
