#pragma once
#include "obj.h"
#include <iostream>
#include <string>

class objNewReader
{
private:

public:
	objNewReader();
	~objNewReader();

	//read file, pass back resulting obj from file
	obj readObjFile(char * inFile);
};
