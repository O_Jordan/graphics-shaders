Program renders an object, then utilises shaders to light the object and apply a halftone effect to it.

///////////////////////////////////////
INSTRUCTIONS
///////////////////////////////////////

Use A and S to change dot size!

//////////////////////////////////////
NOTES
//////////////////////////////////////

To view the Blinn-Phong shading without the halftone effect, change the PASS variable in Globals.h to 1!